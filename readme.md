POC ARCHUNIT

[[_TOC_]]

## Archunit sum up
ArchUnit is a free, simple and extensible library for checking the architecture of your Java code.   
That is, ArchUnit can check dependencies between packages and classes, layers and slices, check for  
cyclic dependencies and more. It does so by analyzing given Java bytecode, importing all classes   
into a Java code structure. ArchUnit’s main focus is to automatically test architecture and coding rules,  
using any plain Java unit testing framework.

## Resources
- <https://www.archunit.org/userguide/html/000_Index.html>

## How does it work ?
Build will fail if the project is not implemented according to architecture rules defined in Archunit tests classes.

## Poc project
This is a Spring Boot project using a simple H2 embedded database

## Technical implementation of Archunit
- artifact in pom.xml : archunit-junit5
- test class : com/example/demo/ArchunitApplicationTests.java

## Try fo fail some architecture tests to see Archunit in action
- move enum class Prenom to another package
- change the ending name of a classes in service package


## Trying controllers with web browser
- http://localhost:8080
- http://localhost:8080/etudiant/all
- http://localhost:8080/etudiant?id=1
- http://localhost:8080/etudiant?id=2 ...

## Access GUI to play with database
H2 console access : http://localhost:8080/h2-console  

- Database JBDC URL : jdbc:h2:mem:testdb
- User Name : sa
- Password : ***it's an empty password, leave it empty***

<img src="h2_database_console_login.png" alt= “s1” width="60%" height="60%">  

All good ! :thumbsup: 



