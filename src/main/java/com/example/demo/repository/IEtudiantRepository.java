package com.example.demo.repository;

import com.example.demo.entity.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
 public interface IEtudiantRepository extends JpaRepository<Etudiant,Long> {
    List<Etudiant> findByNom(String nom);
    List<Etudiant> findByNomContains(String valeur);
    List<Etudiant> findByPrenom(String prenom);

    @Query("select e from Etudiant e where e.nom like :x ")  // x est le paramètre
    List<Etudiant> mySpecificQuery(@Param("x") String nom); // Param créé un lien entre le paramètre Query et la signature de la méthode

}
